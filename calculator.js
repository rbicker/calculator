
/**
 * add 2 numbers
 * @param {Number} a
 * @param {Number} b
 */
const addition = (a, b) => a + b;

module.exports = {
  addition,
};
