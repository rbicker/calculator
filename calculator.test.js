const { expect } = require('chai');

const { addition } = require('./calculator');

/* global describe it :true */
describe('addition', () => {
  it('should add 2 numbers', () => {
    expect(addition(1, 2)).to.be.equal(3);
  });
});
