# stage 1: build client
FROM node:carbon-alpine
WORKDIR /usr/src/app
COPY package* ./
COPY index.js ./
COPY calculator.js ./
RUN npm install
CMD [ "npm", "start" ]